Source: kdotool
Section: utils
Priority: optional
Build-Depends: cargo,
               cmake,
               debcargo,
               debhelper-compat (= 13),
               dh-cargo,
               libdbus-1-dev,
               pkg-config,
               rustc
Standards-Version: 4.6.2
Maintainer: Neon CI <neon@kde.org>

Package: kdotool
Architecture: any
Multi-Arch: same
Depends: cargo, 
         rustc, 
         ${misc:Depends}, 
         ${shlibs:Depends}
Description: a xdotool clone for KDE Wayland
 Wayland, for security concerns, removed most of the X11 APIs that xdotool uses
 to simulate user input and control windows. ydotool solves the input part by
 talking directly to the kernel input device. However, for the window control
 part, you have to use each Wayland compositor's own APIs.
 .
 This program uses KWin's scripting API to control windows. In each invocation,
 it generates a KWin script on-the-fly, loads it into KWin, runs it, and then
 deletes it, using KWin's DBus interface.
 .
 This program should work with both KDE 5 and the upcoming KDE 6. It should
 work with both Wayland and X11 sessions. (But you can use the original xdotool
 in X11, anyway. So this is mainly for Wayland.)
 .
 Not all xdotool commands are supported. Some are not available through the
 KWin API. Some might be not even possible in Wayland. See below for details.
 .
 Please note that the window id this program uses is KWin's internal window id,
 which looks like a UUID ({xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}). It's not a
 X11 window id.
